from peewee import TextField, ForeignKeyField, DateTimeField, FloatField, IntegerField, BooleanField, TimeField, Model
from playhouse.postgres_ext import PostgresqlDatabase, BinaryJSONField
from os import environ
from datetime import datetime
from uuid import uuid4
from time import sleep

host = environ.get("db_host", "0.0.0.0")
pw = environ.get("db_pw", "mysecretpassword")

sleep(5)
database = PostgresqlDatabase('postgres', **{'host': host, 'user': 'postgres', 'password': pw}, connect_timeout=60)


class UnknownField(object):
    def __init__(self, *_, **__): pass


class BaseModel(Model):
    class Meta:
        database = database


class Zone(BaseModel):
    description = TextField()
    name = TextField()
    parent = ForeignKeyField(column_name='parent', field='id', model='self', null=True)
    url = TextField()
    def __str__(self):
        return self.name
    class Meta:
        table_name = 'zone'
try:
    Zone.create_table()
except:
    raise


class Nodule(BaseModel):
    created_at = DateTimeField()
    err_log_size = IntegerField(null=True)
    hw_type = TextField(null=True)
    lat = FloatField(null=True)
    log_size = IntegerField(null=True)
    lon = FloatField(null=True)
    name = TextField()
    power = FloatField(null=True)
    presence = BooleanField(null=True)
    tags = TextField(null=True)
    topics = BinaryJSONField(null=True)
    uid = TextField(unique=True)
    woken_at = DateTimeField(null=True)
    zone = ForeignKeyField(column_name='zone', field='id', model=Zone)
    def __str__(self):
        return '%s - %s' % (self.name, self.uid)
    class Meta:
        table_name = 'nodule'
try:
    Nodule.create_table()
except:
    raise


class Component(BaseModel):
    component_type = TextField()
    description = TextField()
    kind = TextField()
    name = TextField()
    nodule = ForeignKeyField(column_name='nodule', field='id', model=Nodule)
    pin = TextField()
    uid = TextField(unique=True)
    def __str__(self):
        return '%s - %s' % (self.name, self.uid)
    class Meta:
        table_name = 'component'
try:
    Component.create_table()
except:
    raise


class Job(BaseModel):
    at_time = TimeField(null=True)
    component = ForeignKeyField(column_name='component', field='id', model=Component)
    description = TextField()
    # interval = IntegerField(null=True)
    cron = BinaryJSONField()
    kind = TextField()
    name = TextField()
    nodule = ForeignKeyField(column_name='nodule', field='id', model=Nodule)
    params = BinaryJSONField(null=True)
    period = IntegerField(null=True)
    tags = TextField(null=True)
    uid = TextField(unique=True)
    units = TextField(null=True)
    last_triggered = DateTimeField(null=True)
    def __str__(self):
        return '%s - %s' % (self.name, self.uid)
    class Meta:
        table_name = 'job'
try:
    Job.create_table()
except:
    raise


if len(list(Nodule.select())) == 0:
    print("No data in DB")
else:
    print("DB already provisioned")

for x in [Nodule, Job, Component]:
    for n in x.select():
        n.delete()
